package com.example.android.login;

import android.app.AlertDialog;
import android.content.ContentValues;

import android.content.DialogInterface;
import android.database.Cursor;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import android.support.v4.app.NavUtils;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.login.data.LoginContract.LoginEntry;
import com.example.android.login.data.LoginDbHelper;



public class AddTeacher extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {
    /** Identifier for the pet data loader */
    private static final int EXISTING_PET_LOADER = 0;
    private Uri mCurrentTeacherUri;
    private EditText mUsername;
    private EditText mName;
    private EditText mPassword;
    private EditText mSalary;
    private EditText mEmail;
    private EditText mPhone_No;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_teacher);

        Intent intent = getIntent();
       mCurrentTeacherUri = intent.getData();
        if (mCurrentTeacherUri == null) {
            setTitle("Add a Teacher");
        } else {
            setTitle("Edit Teacher");
            getLoaderManager().initLoader(EXISTING_PET_LOADER, null,this);
        }

        // Find all relevant views that we will need to read user input from
        mName = (EditText) findViewById(R.id.et_name);
        mUsername = (EditText) findViewById(R.id.et_Username);
        mPassword = (EditText) findViewById(R.id.et_Password);
        mSalary = (EditText) findViewById(R.id.et_Salary);
        mEmail = (EditText) findViewById(R.id.et_Email);
        mPhone_No = (EditText) findViewById(R.id.et_Phone_NO);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_addteacher, menu);
        return true;
    }

    private void insertTeacher() {
        String nameString = mName.getText().toString().trim();
        String usernameString = mUsername.getText().toString().trim();
        String passwordString = mPassword.getText().toString().trim();
        String salaryString = mSalary.getText().toString().trim();
        int salary = Integer.parseInt(salaryString);
        String emailString = mEmail.getText().toString().trim();
        String phone_noString = mPhone_No.getText().toString().trim();
        //int phone_no = Integer.parseInt(phone_noString);

        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        values.put(LoginEntry.TCOLUMN_NAME, nameString);
        values.put(LoginEntry.TCOLUMN_USERNAME, usernameString);
        values.put(LoginEntry.TCOLUMN_PASSWORD, passwordString);
        values.put(LoginEntry.TCOLUMN_SALARY, salary);
        values.put(LoginEntry.TCOLUMN_EMAIL, emailString);
        values.put(LoginEntry.TCOLUMN_PHONE, phone_noString);


// Determine if this is a new or existing pet by checking if mCurrentPetUri is null or not
        if (mCurrentTeacherUri == null) {
            // This is a NEW pet, so insert a new pet into the provider,
            // returning the content URI for the new pet.
        // Insert a new pet into the provider, returning the content URI for the new pet.
               Uri newUri = getContentResolver().insert(LoginEntry.CONTENT_URI, values);
                        // Show a toast message depending on whether or not the insertion was successful
                                if (newUri == null) {
                                    // If the new content URI is null, then there was an error with insertion.
                                    Toast.makeText(this, getString(R.string.editor_insert_teacher_failed),
                                            Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    // Otherwise, the insertion was successful and we can display a toast.
                                    Toast.makeText(this, getString(R.string.editor_insert_teacher_successful),
                                            Toast.LENGTH_SHORT).show();
                                }
    } else {
        // Otherwise this is an EXISTING pet, so update the pet with content URI: mCurrentPetUri
        // and pass in the new ContentValues. Pass in null for the selection and selection args
        // because mCurrentPetUri will already identify the correct row in the database that
        // we want to modify.
        int rowsAffected = getContentResolver().update(mCurrentTeacherUri, values, null, null);

        // Show a toast message depending on whether or not the update was successful.
        if (rowsAffected == 0) {
            // If no rows were affected, then there was an error with the update.
            Toast.makeText(this, getString(R.string.editor_update_teacher_failed),
                    Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the update was successful and we can display a toast.
            Toast.makeText(this, getString(R.string.editor_update_teacher_successful),
                    Toast.LENGTH_SHORT).show();
        }
    }
        // Insert a new row for Toto in the database, returning the ID of that new row.
        // The first argument for db.insert() is the pets table name.
        // The second argument provides the name of a column in which the framework
        // can insert NULL in the event that the ContentValues is empty (if
        // this is set to "null", then the framework will not insert a row when
        // there are no values).
        // The third argument is the ContentValues object containing the info for Toto.
       /* long newRowId = db.insert(LoginEntry.TTABLE_NAME, null, values);
        Log.v("AddTeacher", "New Row ID:" + newRowId);
        if (newRowId == -1) {
            Toast.makeText(this, "Error with saving Teacher", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Teacher saved with id:" + newRowId, Toast.LENGTH_SHORT).show();
        }*/
        // Determine if this is a new or existing pet by checking if mCurrentPetUri is null or not
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                insertTeacher();
                //Exit activity
                finish();
                // Do nothing for now
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                // Do nothing for now
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_teacher_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deletePet();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * Perform the deletion of the pet in the database.
     */
    private void deletePet() {
        // Only perform the delete if this is an existing pet.
        if (mCurrentTeacherUri != null) {
            // Call the ContentResolver to delete the pet at the given content URI.
            // Pass in null for the selection and selection args because the mCurrentPetUri
            // content URI already identifies the pet that we want.
            int rowsDeleted = getContentResolver().delete(mCurrentTeacherUri, null, null);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {
                // If no rows were deleted, then there was an error with the delete.
                Toast.makeText(this, getString(R.string.editor_delete_teacher_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the delete was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_delete_teacher_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }

        // Close the activity
        finish();
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Since the editor shows all pet attributes, define a projection that contains
        // all columns from the pet table
        // you will actually use after this query.
        String[] projection = {
                LoginEntry._ID,
                LoginEntry.TCOLUMN_NAME,
                LoginEntry.TCOLUMN_USERNAME,
                LoginEntry.TCOLUMN_PASSWORD,
                LoginEntry.TCOLUMN_EMAIL,
                LoginEntry.TCOLUMN_SALARY,
                LoginEntry.TCOLUMN_PHONE};
// This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
               mCurrentTeacherUri,         // Query the content URI for the current pet
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
// Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of pet attributes that we're interested in
            int nameColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_NAME);
            int usernameColumnIndex=cursor.getColumnIndex(LoginEntry.TCOLUMN_USERNAME);
            int passwordColumnIndex=cursor.getColumnIndex(LoginEntry.TCOLUMN_PASSWORD);
            int emailColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_EMAIL);
            int salaryColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_SALARY);
            int phoneColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_PHONE);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            String username=cursor.getString(usernameColumnIndex);
            String password=cursor.getString(passwordColumnIndex);
            String email = cursor.getString(emailColumnIndex);
            int salary = cursor.getInt(salaryColumnIndex);
           // int phone = cursor.getInt(phoneColumnIndex);
            String  phone = cursor.getString(phoneColumnIndex);
            // Update the views on the screen with the values from the database
            mName.setText(name);
            mUsername.setText(username);
            mPassword.setText(password);
            mEmail.setText(email);
            mSalary.setText(Integer.toString(salary));
           // mPhone_No.setText(Integer.toString(phone));
            mPhone_No.setText(phone);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mName.setText("");
        mUsername.setText("");
        mPassword.setText("");
        mEmail.setText("");
        mSalary.setText("");
        mPhone_No.setText("");
    }
}



