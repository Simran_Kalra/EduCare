package com.example.android.login;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class TeacherActivity extends AppCompatActivity {

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_teacher);
        }
        public void Marksheet(View view){
            Intent intent=new Intent(this,MarksheetActivity.class);
            startActivity(intent);
        }

        public void Attendance(View view){
            Intent intent=new Intent(this,AttendanceActivity.class);
            startActivity(intent);
        }

       public void Announcement(View view){
           Intent intent=new Intent(this,AnnouncementActivity.class);
          startActivity(intent);
       }
    }
