package com.example.android.login;


import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.login.data.LoginDbHelper;

import java.util.ArrayList;
import java.util.List;


public class AnnouncementActivity extends AppCompatActivity{

    private MyCustomAdapter myAdapter;
    LoginDbHelper mDbHelper;
    private List nameTitles;
    private final String tablename ="Student";
    String section;
    String title;
    String emailId="";
    String emailText;
    String messageText;
    String subj;
    ListView listview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);


        // Spinner element for selecting class
        final Spinner Class = (Spinner) findViewById(R.id.Class);
        Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                section = Class.getSelectedItem().toString();
                check(section);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    void check(String section)
    {
        // set the ArrayAdapter for the result list
        ArrayList<Name> nameTitles = new ArrayList<Name>();
        myAdapter = new MyCustomAdapter(this, R.layout.row_layout, nameTitles);
        ArrayList results = new ArrayList();


        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT name,roll from STUDENT WHERE class='" + section + "'", null);

        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                do {
                    // Get version from Cursor
                    String nameString = cursor.getString(cursor.getColumnIndex("name"));
                    String rollString = cursor.getString(cursor.getColumnIndex("roll"));
                    // add the bookName into the bookTitles ArrayList
                    Name name = new Name(rollString, nameString, false);
                    nameTitles.add(name);
                    // move to next row
                } while (cursor.moveToNext());
            }
        }

        // initiate the listadapter
        myAdapter = new MyCustomAdapter(this, R.layout.row_layout, nameTitles);
        ListView listview = (ListView) findViewById(android.R.id.list);
        // assign the list adapter
        //setListAdapter(myAdapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listview.setAdapter(myAdapter);
        checkButtonClick();
    }

    private class MyCustomAdapter extends ArrayAdapter<Name> {

        private ArrayList<Name> nameTitles;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Name> nameTitles) {
            super(context, textViewResourceId, nameTitles);
            this.nameTitles = new ArrayList<Name>();
            this.nameTitles.addAll(nameTitles);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.row_layout, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.listText);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBoxTextView1);
                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        Name name = (Name) cb.getTag();
                        Toast.makeText(getApplicationContext(),
                                "Student: " + cb.getText() +
                                        " is " + "absent",
                                Toast.LENGTH_SHORT).show();
                        name.setSelected(cb.isChecked());
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Name name = nameTitles.get(position);
            holder.code.setText(" (" +  name.getCode() + ")");
            holder.name.setText(name.getName());
            holder.name.setChecked(name.isSelected());
            holder.name.setTag(name);

            return convertView;

        }

    }

    private void checkButtonClick() {
       final LoginDbHelper mDbHelper = new LoginDbHelper(this);
        Button myButton = (Button) findViewById(R.id.findSelected);
        myButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText emailSubject = (EditText) findViewById(R.id.tv_announcement);
                emailText = emailSubject.getText().toString().trim();


                StringBuffer responseText = new StringBuffer();
                responseText.append("The following were absent...\n");

                ArrayList<Name> nameTitles = myAdapter.nameTitles;
                for (int i = 0; i < nameTitles.size(); i++) {
                    Name name = nameTitles.get(i);
                    if (name.isSelected()) {
                        SQLiteDatabase db = mDbHelper.getReadableDatabase();
                        Cursor cursor = db.rawQuery("SELECT email_id from STUDENT WHERE name='" + name.getName() + "'", null);
                        // if Cursor is contains results
                        if (cursor != null) {
                            // move cursor to first row
                            if (cursor.moveToFirst()) {

                                emailId=emailId+','+cursor.getString(cursor.getColumnIndex("email_id"));

                            }
                        }
                    }


                }
                messageText = emailText;
                String [] emailList = emailId.split(",");
                ArrayList<String> list = new ArrayList<String>();
                for (String s : emailList)
                    if (!s.equals(""))
                        list.add(s);

                emailList= list.toArray(new String[list.size()]);
                subj = "This is announcement for " + section;
                composeEmail(emailList, messageText, subj);
            }
    });
    }


    public void composeEmail(String[] addresses,String Text,String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL,addresses);
        intent.putExtra(Intent.EXTRA_TEXT, Text);
        intent.putExtra(Intent.EXTRA_SUBJECT,subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
