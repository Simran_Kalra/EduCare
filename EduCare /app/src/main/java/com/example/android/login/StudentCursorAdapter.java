package com.example.android.login;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.login.data.LoginContract.LoginEntry;

public class StudentCursorAdapter extends CursorAdapter {


    public StudentCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml
        return LayoutInflater.from(context).inflate(R.layout.list_item,parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find individual views that we want to modify in the list item layout
        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView PhoneTextView = (TextView) view.findViewById(R.id.phone_no);

        // Find the columns of pet attributes that we're interested in
        int nameColumnIndex = cursor.getColumnIndex(LoginEntry.SCOLUMN_NAME);
        int phoneColumnIndex = cursor.getColumnIndex(LoginEntry.SCOLUMN_PHONE);

        // Read the pet attributes from the Cursor for the current pet
        String Name = cursor.getString(nameColumnIndex);
        String Phone = cursor.getString(phoneColumnIndex);

        // Update the TextViews with the attributes for the current pet
        nameTextView.setText(Name);
        PhoneTextView.setText(Phone);
    }
}
