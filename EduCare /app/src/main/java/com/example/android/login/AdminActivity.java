package com.example.android.login;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import com.example.android.login.data.LoginContract.LoginEntry;


public class AdminActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{
    /** Adapter for the ListView */
    UserCursorAdapter mCursorAdapter;
    /** Identifier for the pet data loader */
    private static final int TEACHER_LOADER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);



/*    @Override
    protected void onStart() {
        super.onStart();
        displayDatabaseInfo();
    }*/

    /**
     * Temporary helper method to display information in the onscreen TextView about the state of
     * the pets database.
     */
/*
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                LoginEntry._ID,
                LoginEntry.TCOLUMN_NAME,
                LoginEntry.TCOLUMN_EMAIL,
                LoginEntry.TCOLUMN_SALARY,
                LoginEntry.TCOLUMN_PHONE};

        Cursor cursor = db.query(
                LoginEntry.TTABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);
       /* //TextView displayView = (TextView) findViewById(R.id.tv_teacherinfo);

        try {
            // Create a header in the Text View that looks like this:
            //
            // The pets table contains <number of rows in Cursor> pets.
            // _id - name - breed - gender - weight
            //
            // In the while loop below, iterate through the rows of the cursor and display
            // the information from each column in this order.
            displayView.setText("The teacher table contains " + cursor.getCount() + " teachers.\n\n");
            displayView.append(LoginEntry._ID + " - " +
                    LoginEntry.TCOLUMN_NAME + " - " +
                    LoginEntry.TCOLUMN_EMAIL + " - " +
                    LoginEntry.TCOLUMN_SALARY + " - " +
                    LoginEntry.TCOLUMN_PHONE + "\n");

            // Figure out the index of each column
            int idColumnIndex = cursor.getColumnIndex(LoginEntry._ID);
            int nameColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_NAME);
            int emailColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_EMAIL);
            int salaryColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_SALARY);
            int phoneColumnIndex = cursor.getColumnIndex(LoginEntry.TCOLUMN_PHONE);

            // Iterate through all the returned rows in the cursor
            while (cursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the cursor is on.
                int currentID = cursor.getInt(idColumnIndex);
                String currentName = cursor.getString(nameColumnIndex);
                String currentEmail = cursor.getString(emailColumnIndex);
                int currentSalary = cursor.getInt(salaryColumnIndex);
                int currentPhone = cursor.getInt(phoneColumnIndex);
                // Display the values from each column of the current row in the cursor in the TextView
                displayView.append(("\n" + currentID + " - " +
                        currentName + " - " +
                        currentEmail + " - " +
                        currentSalary + " - " +
                        currentPhone));
            }
        } finally {
            // Always close the cursor when you're done reading from it. This releases all its
            // resources and makes it invalid.
            cursor.close();
        }
    }
        ListView listView = (ListView) findViewById(R.id.tv_list);
        UserCursorAdapter userCursorAdapter = new UserCursorAdapter(this,cursor);
        listView.setAdapter(userCursorAdapter);
    */

        ListView listView = (ListView) findViewById(R.id.tv_list);
        // Setup an Adapter to create a list item for each row of pet data in the Cursor.
        // There is no pet data yet (until the loader finishes) so pass in null for the Cursor.
        mCursorAdapter = new UserCursorAdapter(this, null);
        listView.setAdapter(mCursorAdapter);


        // Setup the item click listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Create new intent to go to {@link EditorActivity}
                Intent intent = new Intent(AdminActivity.this,AddTeacher.class);

                // Form the content URI that represents the specific pet that was clicked on,
                // by appending the "id" (passed as input to this method) onto the
                // {@link PetEntry#CONTENT_URI}.
                // For example, the URI would be "content://com.example.android.pets/pets/2"
                // if the pet with ID 2 was clicked on.
                Uri currentTeacherUri = ContentUris.withAppendedId(LoginEntry.CONTENT_URI,id);

                // Set the URI on the data field of the intent
                intent.setData(currentTeacherUri);

                // Launch the {@link EditorActivity} to display the data for the current pet.
                startActivity(intent);
            }
        });
        // Kick off the loader
        getLoaderManager().initLoader(TEACHER_LOADER, null,this);
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_adminactivity, menu);
        return true;
    }


    public void addTeacher(MenuItem item) {
        Intent intent = new Intent(this, AddTeacher.class);
        startActivity(intent);
    }
    public void viewStudent(MenuItem item) {
        Intent intent = new Intent(this,ViewStudent.class);
        startActivity(intent);
    }

    public void Logout(MenuItem item){
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
    public void deleteAllTeachers(MenuItem item) {
        int rowsDeleted = getContentResolver().delete(LoginEntry.CONTENT_URI, null, null);
        Log.v("AdminActivity", rowsDeleted + " rows deleted from Teachers database");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert insert teacher" menu option
           case R.id.action_insert_teachers:
              // displayDatabaseInfo();
//               addTeacher();
//               finish();
               return true;
            case R.id.action_view_all_students:
                return true;
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_teachers:
               // deleteAllTeachers();
                // Do nothing for now
                return true;
            // Respond to a click on the "Logout" menu option
            case R.id.action_logout:
                // Do nothing for now
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Define a projection that specifies the columns from the table we care about.
        String[] projection1 = {
                LoginEntry._ID,
                LoginEntry.TCOLUMN_NAME,
                LoginEntry.TCOLUMN_PHONE};

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                LoginEntry.CONTENT_URI,   // Provider content URI to query
                projection1,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update {@link PetCursorAdapter} with this new cursor containing updated pet data
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);
    }
}
