package com.example.android.login;

import android.content.Context;
import android.database.Cursor;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.login.data.LoginContract;

/**
 * Created by simranrkalra on 12/27/2016.
 */

public class MarkCursorAdapter extends CursorAdapter {


    public MarkCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml
        return LayoutInflater.from(context).inflate(R.layout.list_marks, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

            // Find individual views that we want to modify in the list item layout
            TextView idTextView = (TextView) view.findViewById(R.id.s_id);
            TextView rollTextView = (TextView) view.findViewById(R.id.s_roll_no);
            TextView nameTextView = (TextView) view.findViewById(R.id.s_name);
            TextView emailTextView = (TextView) view.findViewById(R.id.s_email);


        // Find the columns of Student attributes that we're interested in
            int idColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry._SID);
            int rollColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_ROLL);
            int nameColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_NAME);
            int emailColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_EMAIL);


            // Read the pet attributes from the Cursor for the current pet
            int Id = cursor.getInt(idColumnIndex);
            int Roll = cursor.getInt(rollColumnIndex);
            String Name = cursor.getString(nameColumnIndex);
            String Email = cursor.getString(emailColumnIndex);


            // Update the TextViews with the attributes for the current pet
            idTextView.setText(Integer.toString(Id));
            rollTextView.setText(Integer.toString(Roll));
            nameTextView.setText(Name);
            emailTextView.setText(Email);

        }
    }

