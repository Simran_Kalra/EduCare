package com.example.android.login;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import com.example.android.login.data.LoginDbHelper;

public class LoginActivity extends AppCompatActivity {
    private LoginDbHelper mDbHelper;
    private Cursor cursor;
//    private Cursor cursor2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mDbHelper = new LoginDbHelper(this);
        // Create and/or open a database to read from it
        SQLiteDatabase db=mDbHelper.getReadableDatabase();
    }

    public void submitInfo(View view) {
        CheckData();
    }

    public void CheckData() {
        // Find all relevant views that we will need to read user input from
        EditText mUsername = (EditText) findViewById(R.id.etusername);
        String username = mUsername.getText().toString().trim();

        // Find all relevant views that we will need to read user input from
        EditText mPassword = (EditText) findViewById(R.id.etpassword);
        String password = mPassword.getText().toString().trim();

        // check if any of the fields are vaccant
        if (username.equals("") || password.equals("")) {
            Toast.makeText(getApplicationContext(), "Field Vaccant", Toast.LENGTH_LONG).show();
            return;
        }
        // fetch the Password form database for respective user name from Admin Table
        String stored_Admin_Password = getSinlgeAdminEntry(username);

        // fetch the Password form database for respective user name from Teachers Table
        String stored_Teacher_Password = getSinlgeTeacherEntry(username);

        // fetch the Password form database for respective user name from Teachers Table
        String stored_Student_Password = getSinlgeStudentEntry(username);

        // check if the Stored password matches with  Password entered by user
        if (password.equals(stored_Admin_Password)) {
            Toast.makeText(LoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, AdminActivity.class);
            startActivity(i);
        } else if (password.equals(stored_Teacher_Password)) {
            Toast.makeText(LoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, TeacherActivity.class);
            startActivity(i);
        } else if (password.equals(stored_Student_Password)) {
            Toast.makeText(LoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, StudentActivity.class);
            Bundle b=new Bundle();
            b.putString("username",username);
            i.putExtras(b);
            startActivity(i);
        } else {
            Toast.makeText(LoginActivity.this, "User Name or Password does not match", Toast.LENGTH_SHORT).show();
        }
    }

    // Create a ContentValues object where column names are the keys,
    // and Toto's pet attributes are the values.

    public String getSinlgeAdminEntry(String username) {
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        cursor = db.query("Admin", null, "username=?", new String[]{username}, null, null, null);
        if (cursor.getCount() < 1) // Username Not Exist
        {
            return "NOT EXIST";
        }
        else {
            cursor.moveToFirst();
            String Apassword = cursor.getString(cursor.getColumnIndex("password"));
            cursor.close();
            return Apassword;
        }
    }

    public String getSinlgeTeacherEntry(String username) {
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        cursor = db.query("Teacher", null, "username=?", new String[]{username}, null, null, null);
        if (cursor.getCount() < 1) // Username Not Exist
        {
            return "NOT EXIST";
        }
        else {
            cursor.moveToFirst();
            String Tpassword = cursor.getString(cursor.getColumnIndex("password"));
            return Tpassword;
        }
    }

    public String getSinlgeStudentEntry(String username) {
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        cursor = db.query("Student", null, "username=?", new String[]{username}, null, null, null);
        if (cursor.getCount() < 1) // Username Not Exist
        {
            return "NOT EXIST";
        }
        else {
            cursor.moveToFirst();
            String Spassword = cursor.getString(cursor.getColumnIndex("password"));
            return Spassword;
        }
    }
}

