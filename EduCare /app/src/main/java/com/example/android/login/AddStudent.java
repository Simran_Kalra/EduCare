package com.example.android.login;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.login.data.LoginContract;
import com.example.android.login.data.LoginDbHelper;

/**
 * Created by simranrkalra on 12/26/2016.
 */

public class AddStudent extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final int EXISTING_STUDENT_LOADER = 0;
    private Uri mCurrentStudentUri;

    private EditText mName;
    private EditText mClass;
    private EditText mRoll;
    private EditText mUsername;
    private EditText mPassword;
    private EditText mEmail;
    private EditText mPhone_No;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        Intent intent = getIntent();
        mCurrentStudentUri = intent.getData();
        if (mCurrentStudentUri == null) {
            setTitle("Add a Student");
        } else {
            setTitle("Edit Student");
            getLoaderManager().initLoader(EXISTING_STUDENT_LOADER, null,this);
        }

        // Find all relevant views that we will need to read user input from
        mName = (EditText) findViewById(R.id.et_name);
        mClass = (EditText) findViewById(R.id.et_class);
        mRoll = (EditText) findViewById(R.id.et_roll);
        mUsername = (EditText) findViewById(R.id.et_Username);
        mPassword = (EditText) findViewById(R.id.et_Password);
        mEmail = (EditText) findViewById(R.id.et_Email);
        mPhone_No = (EditText) findViewById(R.id.et_Phone_NO);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_addstudent, menu);
        return true;
    }
    public void insertStudent(){
        String nameString = mName.getText().toString().trim();
        String classString=mClass.getText().toString().trim();
        String rollString=mRoll.getText().toString().trim();
        int roll=Integer.parseInt(rollString);
        String usernameString = mUsername.getText().toString().trim();
        String passwordString = mPassword.getText().toString().trim();
        String emailString = mEmail.getText().toString().trim();
        String phone_noString = mPhone_No.getText().toString().trim();
        //int phone_no = Integer.parseInt(phone_noString);

        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        values.put(LoginContract.LoginEntry.SCOLUMN_NAME, nameString);
        values.put(LoginContract.LoginEntry.SCOLUMN_CLASS,classString);
        values.put(LoginContract.LoginEntry.SCOLUMN_ROLL,roll);
        values.put(LoginContract.LoginEntry.SCOLUMN_USERNAME, usernameString);
        values.put(LoginContract.LoginEntry.SCOLUMN_PASSWORD, passwordString);
        values.put(LoginContract.LoginEntry.SCOLUMN_EMAIL, emailString);
        values.put(LoginContract.LoginEntry.SCOLUMN_PHONE, phone_noString);
       /* long newRowId = db.insert(LoginContract.LoginEntry.STABLE_NAME, null, values);
        Log.v("AddStudent", "New Row ID:" + newRowId);
        if (newRowId == -1) {
            Toast.makeText(this, "Error with saving Student", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Student saved with id:" + newRowId, Toast.LENGTH_SHORT).show();
        }*/
        if(mCurrentStudentUri==null){
            Uri newUri = getContentResolver().insert(LoginContract.LoginEntry.SCONTENT_URI, values);

        // Show a toast message depending on whether or not the insertion was successful
        if (newUri == null) {
            // If the new content URI is null, then there was an error with insertion.
            Toast.makeText(this, getString(R.string.editor_insert_student_failed),
                    Toast.LENGTH_SHORT).show();
        }
        else {
            // Otherwise, the insertion was successful and we can display a toast.
            Toast.makeText(this, getString(R.string.editor_insert_student_successful),
                    Toast.LENGTH_SHORT).show();
        }
        }
        else{
            int rowsAffected = getContentResolver().update(mCurrentStudentUri, values, null, null);

            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, getString(R.string.editor_update_student_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_update_student_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                insertStudent();
                //Exit activity
                finish();
                // Do nothing for now
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                // Do nothing for now
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_student_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Delete" button, so delete the pet.
                deleteStudent();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

        /**
         * Perform the deletion of the pet in the database.
         */
    private void deleteStudent() {
        // Only perform the delete if this is an existing pet.
        if (mCurrentStudentUri != null) {
            // Call the ContentResolver to delete the pet at the given content URI.
            // Pass in null for the selection and selection args because the mCurrentPetUri
            // content URI already identifies the pet that we want.
            int rowsDeleted = getContentResolver().delete(mCurrentStudentUri, null, null);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {
                // If no rows were deleted, then there was an error with the delete.
                Toast.makeText(this, getString(R.string.editor_delete_student_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the delete was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_delete_student_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        // Close the activity
        finish();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Since the editor shows all pet attributes, define a projection that contains
        // all columns from the pet table
        // you will actually use after this query.
        String[] projection = {
                LoginContract.LoginEntry._SID,
                LoginContract.LoginEntry.SCOLUMN_NAME,
                LoginContract.LoginEntry.SCOLUMN_CLASS,
                LoginContract.LoginEntry.SCOLUMN_ROLL,
                LoginContract.LoginEntry.SCOLUMN_USERNAME,
                LoginContract.LoginEntry.SCOLUMN_PASSWORD,
                LoginContract.LoginEntry.SCOLUMN_EMAIL,
                LoginContract.LoginEntry.SCOLUMN_PHONE};
// This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                mCurrentStudentUri,         // Query the content URI for the current pet
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
// Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of pet attributes that we're interested in
            int nameColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_NAME);
            int classColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_CLASS);
            int rollColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_ROLL);
            int usernameColumnIndex=cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_USERNAME);
            int passwordColumnIndex=cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_PASSWORD);
            int emailColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_EMAIL);
            int phoneColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_PHONE);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            String std= cursor.getString(classColumnIndex);
            int roll=cursor.getInt(rollColumnIndex);
            String username=cursor.getString(usernameColumnIndex);
            String password=cursor.getString(passwordColumnIndex);
            String email = cursor.getString(emailColumnIndex);
            //int phone = cursor.getInt(phoneColumnIndex);
            String phone = cursor.getString(phoneColumnIndex);

            // Update the views on the screen with the values from the database
            mName.setText(name);
            mClass.setText(std);
            mRoll.setText(Integer.toString(roll));
            mUsername.setText(username);
            mPassword.setText(password);
            mEmail.setText(email);
            //mPhone_No.setText(Integer.toString(phone));
            mPhone_No.setText(phone);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mName.setText("");
        mClass.setText("");
        mRoll.setText("");
        mUsername.setText("");
        mPassword.setText("");
        mEmail.setText("");
        mPhone_No.setText("");
    }
}
