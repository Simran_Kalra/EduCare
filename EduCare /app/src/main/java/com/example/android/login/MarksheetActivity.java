package com.example.android.login;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.android.login.data.LoginContract;
import com.example.android.login.data.LoginDbHelper;

import java.util.ArrayList;
import java.util.List;


public class MarksheetActivity extends AppCompatActivity {
    String section;
    String exam;
    String subj;
    Cursor todoCursor;
    MarkCursorAdapter todoAdapter;
    ListView lvItems;
    Spinner Class, Subject;
    /**
     * Identifier for the pet data loader
     */
    private static final int MARK_LOADER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marksheet);

        final Spinner Subject = (Spinner) findViewById(R.id.Subject);
        // Spinner element for selecting exam
        final Spinner Exam = (Spinner) findViewById(R.id.Exam);
        Exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                exam = Exam.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // Spinner element for selecting class
        final Spinner Class = (Spinner) findViewById(R.id.Class);
        Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String sp1= String.valueOf(Class.getSelectedItem());
                Toast.makeText(MarksheetActivity.this, sp1, Toast.LENGTH_SHORT).show();
                if(sp1.contentEquals("CO1G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("English");
                    list.add("Basic Science");
                    list.add("Basic Mathematics");
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MarksheetActivity.this,
                            android.R.layout.simple_spinner_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter);
                }
                if(sp1.contentEquals("CO2G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("Communication Skills");
                    list.add("Applied Science");
                    list.add("Programming in C");
                    list.add("Basic Electronics");
                    list.add("Mathematics");
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(MarksheetActivity.this,android.R.layout.simple_spinner_item,list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter2);
                }
                if(sp1.contentEquals("CO3G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("Data Structure using C");
                    list.add("Electrical Technology");
                    list.add("RDBMS");
                    list.add("Digital Techniques");
                    list.add("Applied Mathematics");
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(MarksheetActivity.this,android.R.layout.simple_spinner_item,list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter2);
                }
                if(sp1.contentEquals("CO4G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("Computer Hardware");
                    list.add("Computer Networks");
                    list.add("Microprocessor");
                    list.add("OOP");
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(MarksheetActivity.this,android.R.layout.simple_spinner_item,list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter2);
                }
                if(sp1.contentEquals("CO5G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("Operating System");
                    list.add("S/W Engineering");
                    list.add("Computer Security");
                    list.add("Java Programming");
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(MarksheetActivity.this,android.R.layout.simple_spinner_item,list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter2);
                }
                if(sp1.contentEquals("CO6G")) {
                    List<String> list = new ArrayList<String>();
                    list.add("Management");
                    list.add("Software Testing");
                    list.add("Advanced Java Programming");
                    list.add("Advanced Microprocessor");
                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(MarksheetActivity.this,android.R.layout.simple_spinner_item,list);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.notifyDataSetChanged();
                    Subject.setAdapter(dataAdapter2);
                }
                section = Class.getSelectedItem().toString();
                check(section);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                section = Class.getSelectedItem().toString();
                check(section);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        // Spinner element for selecting section
        //final Spinner Subject = (Spinner) findViewById(R.id.Subject);
        Subject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                subj = Subject.getSelectedItem().toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {



        /*@Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }*/
        // Find ListView to populate
        lvItems = (ListView) findViewById(R.id.tv_list);
        //Setup on item click listener (on list)
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(MarksheetActivity.this, AddMarks.class);
                Uri currentStudentUri = ContentUris.withAppendedId(LoginContract.LoginEntry.SCONTENT_URI, id);
                intent.setData(currentStudentUri);
                Bundle b = new Bundle();
                b.putString("Class", section);
                b.putString("Exam", exam);
                b.putString("Subject", subj);
                intent.putExtras(b);
                startActivity(intent);

            }
        });
    }
   /* @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String sub1 = String.valueOf(Class.getSelectedItem());
        // Toast.makeText(this, Class, Toast.LENGTH_SHORT).show();
        if (sub1.equals("CO1G")) {
            List<String> list = new ArrayList<String>();
            list.add("English");
            list.add("Basic Science");
            list.add("Basic Mathematics");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter.notifyDataSetChanged();
            Subject.setAdapter(dataAdapter);
        }

        if(sub1.contentEquals("CO2G")) {
            List<String> list = new ArrayList<String>();
            list.add("Communication Skills");
            list.add("Applied Scienc");
            list.add("Programming in C");
            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataAdapter2.notifyDataSetChanged();
            Subject.setAdapter(dataAdapter2);
        }
    }

    @Override
    public void onNothingSelected (AdapterView <?> adapterView){

    }*/

    public void check(String section) {
        // String section = Class.getSelectedItem().toString();
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        todoCursor = db.rawQuery("SELECT _id,name,roll,email_id from Student WHERE class='" + section + "'", null);


        // Find ListView to populate
        lvItems = (ListView) findViewById(R.id.tv_list);
        // Setup cursor adapter using cursor from last step
        todoAdapter = new MarkCursorAdapter(this, todoCursor);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_mark, menu);
        return true;
    }



 /*   private void insertMark(String exam, String sect, String subj) {
        TextView Id = (TextView) findViewById(R.id.s_id);
        String IdString = Id.getText().toString().trim();
        int id = Integer.parseInt(IdString);


        TextView roll = (TextView) findViewById(R.id.s_roll_no);
        String rollString = roll.getText().toString().trim();
        int roll_no = Integer.parseInt(rollString);

        TextView name = (TextView) findViewById(R.id.s_name);
        String nameString = name.getText().toString().trim();
        TextView email = (TextView) findViewById(R.id.s_email);
        String emailString = email.getText().toString().trim();

        EditText mark = (EditText) findViewById(R.id.s_marks);
        String markString = mark.getText().toString();
        int marks = Integer.parseInt(markString);
        LoginDbHelper mDbHelper=new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        values.put(LoginContract.LoginEntry._MID, id);
        values.put(LoginContract.LoginEntry.MCOLUMN_NAME, nameString);
        values.put(LoginContract.LoginEntry.MCOLUMN_ROLL, roll_no);
        values.put(LoginContract.LoginEntry.MCOLUMN_EMAIL, emailString);
        values.put(LoginContract.LoginEntry.MCOLUMN_MARKS, marks);
        values.put(LoginContract.LoginEntry.MCOLUMN_CLASS, sect);
        values.put(LoginContract.LoginEntry.MCOLUMN_SUBJECT, subj);
        values.put(LoginContract.LoginEntry.MCOLUMN_EXAM, exam);

        long newRowId = db.insert(LoginContract.LoginEntry.MTABLE_NAME, null, values);

            // Determine if this is a new or existing pet by checking if mCurrentPetUri is null or not*/
       /* String textThatYouWantToShare =
                "Marks obtained by " + nameString + " of class " + sect + " in exam " + exam + " is " + marks;

        // COMPLETED (6) Replace the Toast with shareText, passing in the String from step 5
        /* Send that text to our method that will share it. */
    // shareText(textThatYouWantToShare);
    //  }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Do nothing for now
                //  insertMark(exam, section, subj);
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                // Do nothing for now
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }






  /*  private void shareText(String textToShare) {
        // COMPLETED (2) Create a String variable called mimeType and set it to "text/plain"
        /*
         * You can think of MIME types similarly to file extensions. They aren't the exact same,
         * but MIME types help a computer determine which applications can open which content. For
         * example, if you double click on a .pdf file, you will be presented with a list of
         * programs that can open PDFs. Specifying the MIME type as text/plain has a similar affect
         * on our implicit Intent. With text/plain specified, all apps that can handle text content
         * in some way will be offered when we call startActivity on this particular Intent.
         */
    //   String mimeType = "text/plain";

    // COMPLETED (3) Create a title for the chooser window that will pop up
        /* This is just the title of the window that will pop up when we call startActivity */
    //   String title = "Learning How to Share";

    // COMPLETED (4) Use ShareCompat.IntentBuilder to build the Intent and start the chooser
        /* ShareCompat.IntentBuilder provides a fluent API for creating Intents */
    //   ShareCompat.IntentBuilder
                /* The from method specifies the Context from which this share is coming from */
    //            .from(this)
    //          .setType(mimeType)
    //        .setChooserTitle(title)
    //       .setText(textToShare)
    //    .startChooser();
    //}
}


