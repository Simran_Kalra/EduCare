package com.example.android.login;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.login.data.LoginContract;

/**
 * Created by simranrkalra on 1/2/2017.
 */

public class CheckMarkCursorAdapter extends CursorAdapter {


    public CheckMarkCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml
        return LayoutInflater.from(context).inflate(R.layout.list_checkmarks, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find individual views that we want to modify in the list item layout
        TextView idTextView=(TextView)view.findViewById(R.id.tv_Id);
        TextView subjectTextView = (TextView) view.findViewById(R.id.tv_Subject);
        TextView marksTextView = (TextView) view.findViewById(R.id.tv_Marks);



        // Find the columns of Student attributes that we're interested in
        int idColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry._MID);
        int subjectColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.MCOLUMN_SUBJECT);
        int marksColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.MCOLUMN_MARKS);



        // Read the pet attributes from the Cursor for the current pet
        int ID=cursor.getInt(idColumnIndex);
        String Subject = cursor.getString(subjectColumnIndex);
        int Marks = cursor.getInt(marksColumnIndex);



        // Update the TextViews with the attributes for the current pet
        idTextView.setText(Integer.toString(ID));
        marksTextView.setText(Integer.toString(Marks));
        subjectTextView.setText(Subject);


    }
}

