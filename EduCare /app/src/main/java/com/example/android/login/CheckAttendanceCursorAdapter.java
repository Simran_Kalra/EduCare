package com.example.android.login;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.login.data.LoginContract;

/**
 * Created by simranrkalra on 12/31/2016.
 */

public class CheckAttendanceCursorAdapter extends CursorAdapter {
    public CheckAttendanceCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Inflate a list item view using the layout specified in list_item.xml
        return LayoutInflater.from(context).inflate(R.layout.list_checkattendance, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find individual views that we want to modify in the list item layout
        TextView idTextView = (TextView) view.findViewById(R.id.tv_Id);
        TextView dateTextView = (TextView) view.findViewById(R.id.tv_Date);
        TextView attendanceTextView = (TextView) view.findViewById(R.id.tv_Attendance);


        // Find the columns of Student attributes that we're interested in
        int idColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry._ATID);
        int dateColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.ATCOLUMN_DATE);
        int attendanceColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.ATCOLUMN_ATTENDANCE);


        // Read the pet attributes from the Cursor for the current pet
        int ID = cursor.getInt(idColumnIndex);
        String Date = cursor.getString(dateColumnIndex);
        String attendance = cursor.getString(attendanceColumnIndex);


        // Update the TextViews with the attributes for the current pet
        idTextView.setText(Integer.toString(ID));
        dateTextView.setText(Date);
        attendanceTextView.setText(attendance);

    }
}
