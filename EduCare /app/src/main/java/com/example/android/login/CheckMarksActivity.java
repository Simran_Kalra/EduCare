package com.example.android.login;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.android.login.data.LoginDbHelper;

public class CheckMarksActivity extends AppCompatActivity {
    String section;
    String exam;
    private EditText mSubject;
    private EditText mMarks;
    private EditText mExam;
    private EditText mRoll;
    private EditText mName;
    Cursor cursor;
    CheckMarkCursorAdapter todoAdapter;
    ListView listView;
    String username;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_marks);

        Bundle b=getIntent().getExtras();
        username=b.getString("username");

        mRoll = (EditText) findViewById(R.id.et_ROLL);
//        mSubject = (EditText) findViewById(R.id.et_SUBJECT);
//        mMarks = (EditText) findViewById(R.id.et_MARKS);
//        mExam = (EditText) findViewById(R.id.et_EXAM);
        mName=(EditText)findViewById(R.id.et_NAME);
        mName.setText(username);

        // Spinner element for selecting class
        final Spinner Class = (Spinner) findViewById(R.id.Class);
//        EditText et_name = (EditText) findViewById(R.id.et_NAME);
//        name = et_name.getText().toString().trim();
        Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                section = Class.getSelectedItem().toString();
                check(section,username);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        // Spinner element for selecting exam
        final Spinner Exam = (Spinner) findViewById(R.id.Exam);
        Exam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                exam = Exam.getSelectedItem().toString();
               // check(section,username,exam);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void check(String section,String username) {
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT roll,subject,marks,_id,exam from Marks WHERE class='" + section + "' and name='" + username + "' and exam='" + exam + "'", null);
        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                // Get version from Cursor
                int rollString = cursor.getInt(cursor.getColumnIndex("roll"));
                //String subjectString = cursor.getString(cursor.getColumnIndex("subject"));
                //int marksString = cursor.getInt(cursor.getColumnIndex("marks"));
                //String examString = cursor.getString(cursor.getColumnIndex("exam"));


                mRoll.setText(Integer.toString(rollString));
               // mSubject.setText(subjectString);
                //mMarks.setText(Integer.toString(marksString));
                //mExam.setText(examString);

            }
        }
        // Find ListView to populate
        listView = (ListView) findViewById(R.id.list_marks);
        // Setup cursor adapter using cursor from last step
        todoAdapter = new CheckMarkCursorAdapter(this,cursor );
        // Attach cursor adapter to the ListView
        listView.setAdapter(todoAdapter);
    }
    }

