package com.example.android.login;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.android.login.data.LoginContract;

/**
 * Created by simranrkalra on 12/26/2016.
 */

public class ViewStudent extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor>{
    /** Adapter for the ListView */
    StudentCursorAdapter mCursorAdapter;
    /** Identifier for the pet data loader */
    private static final int STUDENT_LOADER = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);

        ListView listView = (ListView) findViewById(R.id.tv_list);
        // Setup an Adapter to create a list item for each row of pet data in the Cursor.
        // There is no pet data yet (until the loader finishes) so pass in null for the Cursor.
        mCursorAdapter = new StudentCursorAdapter(this, null);
        listView.setAdapter(mCursorAdapter);

        //Setup on item click listener (on list)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent=new Intent(ViewStudent.this,AddStudent.class);
                Uri currentStudentUri= ContentUris.withAppendedId(LoginContract.LoginEntry.SCONTENT_URI,id);
                intent.setData(currentStudentUri);
                startActivity(intent);
            }
        });
        // Kick off the loader
        getLoaderManager().initLoader(STUDENT_LOADER, null,this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_viewstudent, menu);
        return true;
    }

    public void addStudent(MenuItem item){
        Intent intent=new Intent(this,AddStudent.class);
        startActivity(intent);
    }

    public void Logout(MenuItem item){
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    private void deleteAllStudents() {
        int rowsDeleted = getContentResolver().delete(LoginContract.LoginEntry.SCONTENT_URI, null, null);
        Log.v("ViewStudent", rowsDeleted + " rows deleted from Student database");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert insert teacher" menu option
            case R.id.action_insert_students:
//               addTeacher();
//               finish();
                return true;
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_students:
                deleteAllStudents();
                // Do nothing for now
                return true;
            // Respond to a click on the "Logout" menu option
            case R.id.action_logout:
                // Do nothing for now
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // Define a projection that specifies the columns from the table we care about.
        String[] projection1 = {
                LoginContract.LoginEntry._SID,
                LoginContract.LoginEntry.SCOLUMN_NAME,
                LoginContract.LoginEntry.SCOLUMN_CLASS,
                LoginContract.LoginEntry.SCOLUMN_ROLL,
                LoginContract.LoginEntry.SCOLUMN_USERNAME,
                LoginContract.LoginEntry.SCOLUMN_PASSWORD,
                LoginContract.LoginEntry.SCOLUMN_EMAIL,
                LoginContract.LoginEntry.SCOLUMN_PHONE};

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                LoginContract.LoginEntry.SCONTENT_URI,   // Provider content URI to query
                projection1,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update {@link PetCursorAdapter} with this new cursor containing updated pet data
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Callback called when the data needs to be deleted
        mCursorAdapter.swapCursor(null);
    }
}
