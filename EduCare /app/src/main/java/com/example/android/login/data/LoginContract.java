package com.example.android.login.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by simranrkalra on 12/20/2016.
 */

public class LoginContract {
    private LoginContract(){}
    public static final String CONTENT_AUTHORITY = "com.example.android.login";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_TEACHERS = "Teacher";

    public static final String PATH_STUDENTS = "Student";




    public static abstract class LoginEntry implements BaseColumns {

        /** The content URI to access the pet data in the provider */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_TEACHERS);
        /** The content URI to access the pet data in the provider */
        public static final Uri SCONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_STUDENTS);


        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String TCONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TEACHERS;
        public static final String SCONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_STUDENTS;


        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String TCONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_TEACHERS;
        public static final String SCONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_STUDENTS;



        public static final String ATABLE_NAME = "Admin";
        public static final String _AID = BaseColumns._ID;
        public static final String ACOLUMN_USERNAME = "username";
        public static final String ACOLUMN_PASSWORD = "password";
        public static final String ACOLUMN_TYPE = "type";

        public static final String TTABLE_NAME = "Teacher";
        public static final String _TID = BaseColumns._ID;
        public static final String TCOLUMN_NAME = "name";
        public static final String TCOLUMN_USERNAME = "username";
        public static final String TCOLUMN_PASSWORD = "password";
        public static final String TCOLUMN_SALARY = "salary";
        public static final String TCOLUMN_EMAIL = "email_id";
        public static final String TCOLUMN_PHONE = "phone_no";
        public static final String TCOLUMN_TYPE = "type";

        public static final String STABLE_NAME="Student";
        public static final String _SID = BaseColumns._ID;
        public static final String SCOLUMN_NAME = "name";
        public static final String SCOLUMN_CLASS="class";
        public static final String SCOLUMN_ROLL="roll";
        public static final String SCOLUMN_USERNAME = "username";
        public static final String SCOLUMN_PASSWORD = "password";
        public static final String SCOLUMN_EMAIL = "email_id";
        public static final String SCOLUMN_PHONE = "phone_no";
        public static final String SCOLUMN_TYPE = "type";


        public static final String MTABLE_NAME="Marks";
        public static final String _MID = "_id";
        public static final String MCOLUMN_NAME = "name";
        public static final String MCOLUMN_ROLL = "roll";
        public static final String MCOLUMN_MARKS = "marks";
        public static final String MCOLUMN_EMAIL = "email_id";
        public static final String MCOLUMN_EXAM = "exam";
        public static final String MCOLUMN_SUBJECT = "subject";
        public static final String MCOLUMN_CLASS = "class";


        public static final String ATTABLE_NAME="Attendance";
        public static final String _ATID = BaseColumns._ID;
        public static final String ATCOLUMN_NAME = "name";
        public static final String ATCOLUMN_CLASS="class";
        public static final String ATCOLUMN_ROLL="roll";
        public static final String ATCOLUMN_ATTENDANCE = "attendance";
        public static final String ATCOLUMN_DATE = "date";
        public static final String ATCOLUMN_MONTH = "month";
    }
}
