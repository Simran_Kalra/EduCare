package com.example.android.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StudentActivity extends AppCompatActivity {
String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        Bundle b=getIntent().getExtras();
        username=b.getString("username");
    }
    public void checkMarks(View view){
        Intent i=new Intent(this,CheckMarksActivity.class);
        Bundle b=new Bundle();
        b.putString("username",username);
        i.putExtras(b);
        startActivity(i);
    }
    public void checkAttendance(View view){
        Intent i=new Intent(this,CheckAttendanceActivity.class);
        Bundle b=new Bundle();
        b.putString("username",username);
        i.putExtras(b);
        startActivity(i);
    }
}
