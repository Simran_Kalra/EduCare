package com.example.android.login;


import android.app.DatePickerDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.login.data.LoginContract;
import com.example.android.login.data.LoginDbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_ATTENDANCE;
import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_CLASS;
import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_DATE;
import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_MONTH;
import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_NAME;
import static com.example.android.login.data.LoginContract.LoginEntry.ATCOLUMN_ROLL;




public class AttendanceActivity extends ListActivity implements View.OnClickListener{
    private MyCustomAdapter myAdapter;
    private List nameTitles;
    private final String tablename ="Student";
    String section;
    String messageString;
    String title;
    String  phoneString;

    String attendance;
    String nameString;
    private SimpleDateFormat dateFormatter;
    private EditText toDateEtxt;
    private DatePickerDialog toDatePickerDialog;
    String rollString;
    int month;
    int day;
    int year;
    int monthInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        toDateEtxt = (EditText) findViewById(R.id.et_date);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        // Spinner element for selecting class
        final Spinner Class = (Spinner) findViewById(R.id.Class);
        Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                section = Class.getSelectedItem().toString();
                check(section);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setDateTimeField();
    }

    private void setDateTimeField() {
        toDateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
//                monthInt=newDate.get(year);
//                month=monthInt+1;
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onClick(View view) {
        if(view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }


    void check(String section)
    {
        // set the ArrayAdapter for the result list
        ArrayList<Name> nameTitles = new ArrayList<Name>();
        myAdapter = new MyCustomAdapter(this, R.layout.row_layout, nameTitles);
        ArrayList results = new ArrayList();


        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT name,roll from STUDENT WHERE class='" + section + "'", null);

        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                do {
                    // Get version from Cursor
                    String nameString = cursor.getString(cursor.getColumnIndex("name"));
                    String rollString = cursor.getString(cursor.getColumnIndex("roll"));
                    // add the bookName into the bookTitles ArrayList
                    Name name = new Name(rollString, nameString, false);
                    nameTitles.add(name);
                    // move to next row
                } while (cursor.moveToNext());
            }
        }

        // initiate the listadapter
        myAdapter = new MyCustomAdapter(this, R.layout.row_layout, nameTitles);
        ListView listview = (ListView) findViewById(android.R.id.list);
        // assign the list adapter
        //setListAdapter(myAdapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listview.setAdapter(myAdapter);
        checkButtonClick();
    }

    private class MyCustomAdapter extends ArrayAdapter<Name> {

        private ArrayList<Name> nameTitles;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Name> nameTitles) {
            super(context, textViewResourceId, nameTitles);
            this.nameTitles = new ArrayList<Name>();
            this.nameTitles.addAll(nameTitles);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.row_layout, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.listText);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBoxTextView1);
                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        Name name = (Name) cb.getTag();
                        Toast.makeText(getApplicationContext(),
                                "Student: " + cb.getText() +
                                        " is " + "absent",
                                Toast.LENGTH_SHORT).show();
                        name.setSelected(cb.isChecked());
                        nameString=name.getName();
                        rollString=name.getCode();
                        attendance="Absent";
                        insertAttendance(rollString,section,nameString,attendance);
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Name name = nameTitles.get(position);
            holder.code.setText(" (" +  name.getCode() + ")");
            holder.name.setText(name.getName());
            holder.name.setChecked(name.isSelected());
            holder.name.setTag(name);

            return convertView;

        }

    }
    private void insertAttendance(String rollString,String section,String name,String attendance)
    {
        EditText dateString=(EditText)findViewById(R.id.et_date);
        String date=dateString.getText().toString();
        String[]dateParts = date.split("-");
        year = Integer.parseInt(dateParts[2]);
        month = Integer.parseInt(dateParts[1]);
        day = Integer.parseInt(dateParts[0]);
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ATCOLUMN_ROLL,rollString);
        values.put(ATCOLUMN_CLASS,section);
        values.put(ATCOLUMN_NAME,name);
        values.put(ATCOLUMN_ATTENDANCE,attendance);
        values.put(ATCOLUMN_DATE,date);
        values.put(ATCOLUMN_MONTH,month);
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        long newRowId = db.insert(LoginContract.LoginEntry.ATTABLE_NAME, null, values);
    }

    private void checkButtonClick() {


        Button myButton = (Button) findViewById(R.id.findSelected);
        myButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                StringBuffer responseText = new StringBuffer();
                responseText.append("The following were absent...\n");

                ArrayList<Name> nameTitles = myAdapter.nameTitles;
                for (int i = 0; i < nameTitles.size(); i++) {
                    Name name = nameTitles.get(i);
                    if (name.isSelected()) {
                        responseText.append("\n" + name.getName());
                        title=name.getName();
                        checkName(title);
                    }
                }

                Toast.makeText(getApplicationContext(),
                        responseText, Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void checkName(String name){
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT phone_no from STUDENT WHERE name='" + name + "'", null);

        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                // do {
                // Get version from Cursor
                phoneString = cursor.getString(cursor.getColumnIndex("phone_no"));
                messageString="Your child " + name +" was absent today.";
                composeMmsMessage(messageString, phoneString);
                // move to next row
                // } while (cursor.moveToNext());
            }
        }
    }

    public void composeMmsMessage(String message,String phoneNumber ) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}



