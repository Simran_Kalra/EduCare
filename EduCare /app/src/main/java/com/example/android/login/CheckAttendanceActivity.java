package com.example.android.login;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.android.login.data.LoginDbHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CheckAttendanceActivity extends AppCompatActivity implements View.OnClickListener{
    String username;
    String section;
    private EditText mRoll;
    private EditText mName;
    Cursor cursor;
    CheckAttendanceCursorAdapter todoAdapter;
    ListView listView;
    private SimpleDateFormat dateFormatter;
    private EditText toDateEtxt;
    private DatePickerDialog toDatePickerDialog;
    String date;
    int month;
    int day;
    String dateString;
    int year;
    int monthInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_attendance);


        dateFormatter = new SimpleDateFormat("MM-yyyy", Locale.US);
        toDateEtxt = (EditText) findViewById(R.id.et_date);
        toDateEtxt.setInputType(InputType.TYPE_NULL);
      //  date=toDateEtxt.getText().toString();


        Bundle b = getIntent().getExtras();
        username = b.getString("username");

        mRoll = (EditText) findViewById(R.id.et_ROLL);
        mName = (EditText) findViewById(R.id.et_NAME);
        mName.setText(username);



        // Spinner element for selecting class
        final Spinner Class = (Spinner) findViewById(R.id.Class);

        Class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                section = Class.getSelectedItem().toString();
                check(section, username,month);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setDateTimeField();
    }
    private void setDateTimeField() {
        toDateEtxt.setOnClickListener(this);
        Calendar newCalendar = Calendar.getInstance();
        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
//                monthInt=newDate.get(year);
//                month=monthInt+1;
                EditText dateString=(EditText)findViewById(R.id.et_date);
                String date=dateString.getText().toString();
                String[]dateParts = date.split("-");
                year = Integer.parseInt(dateParts[1]);
                month = Integer.parseInt(dateParts[0]);
                //day = Integer.parseInt(dateParts[0]);
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    @Override
    public void onClick(View view) {
        if(view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }
    public void check(String section,String username,int month) {
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        cursor = db.rawQuery("SELECT roll,attendance,_id,date from Attendance WHERE class='" + section + "' and name='" + username + "' and month='" + month + "'", null);
        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                // Get version from Cursor
                int rollString = cursor.getInt(cursor.getColumnIndex("roll"));
                mRoll.setText(Integer.toString(rollString));

            }
        }
        // Find ListView to populate
        listView = (ListView) findViewById(R.id.list_attendance);
        // Setup cursor adapter using cursor from last step
        todoAdapter = new CheckAttendanceCursorAdapter(this,cursor);
        // Attach cursor adapter to the ListView
        listView.setAdapter(todoAdapter);
    }
}
