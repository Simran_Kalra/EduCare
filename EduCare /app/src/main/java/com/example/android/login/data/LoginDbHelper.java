package com.example.android.login.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import static com.example.android.login.data.LoginContract.LoginEntry;


/**
 * Created by simranrkalra on 12/20/2016.
 */

public class LoginDbHelper extends SQLiteOpenHelper {
        public static final String LOG_TAG = LoginDbHelper.class.getSimpleName();
        private static final int DATABASE_VERSION = 5;
        private static final String DATABASE_NAME = "User.db";


        public LoginDbHelper(Context context) {
            super(context,DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String CREATE_ADMIN_TABLE = "CREATE TABLE " + LoginEntry.ATABLE_NAME + "("
                    + LoginEntry._AID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + LoginEntry.ACOLUMN_USERNAME + " TEXT NOT NULL, "
                    + LoginEntry.ACOLUMN_PASSWORD + " TEXT NOT NULL);";
            db.execSQL(CREATE_ADMIN_TABLE);
            db.execSQL("INSERT INTO " + LoginEntry.ATABLE_NAME+ "(username,password) VALUES ('Admin','Admin')");

            String CREATE_TEACHER_TABLE = "CREATE TABLE " + LoginEntry.TTABLE_NAME + "("
                    + LoginEntry._TID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + LoginEntry.TCOLUMN_NAME + " TEXT NOT NULL, "
                    + LoginEntry.TCOLUMN_USERNAME + " TEXT NOT NULL, "
                    + LoginEntry.TCOLUMN_PASSWORD + " TEXT NOT NULL, "
                    + LoginEntry.TCOLUMN_SALARY + " INTEGER NOT NULL, "
                    + LoginEntry.TCOLUMN_EMAIL + " TEXT NOT NULL, "
                    + LoginEntry.TCOLUMN_PHONE + " TEXT NOT NULL );";
            db.execSQL(CREATE_TEACHER_TABLE);
            db.execSQL("INSERT INTO " + LoginEntry.TTABLE_NAME+ "(name,username,password,salary,email_id,phone_no) VALUES ('Pratibha','Pratibha','Pratibha123','1200','pratibha@gmail.com','9820787332')");


            String CREATE_STUDENT_TABLE = "CREATE TABLE " +LoginEntry.STABLE_NAME +"("
                    + LoginEntry._SID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + LoginEntry.SCOLUMN_NAME + " TEXT NOT NULL, "
                    + LoginEntry.SCOLUMN_CLASS + " TEXT NOT NULL, "
                    + LoginEntry.SCOLUMN_ROLL + " INTEGER NOT NULL, "
                    + LoginEntry.SCOLUMN_USERNAME + " TEXT NOT NULL, "
                    + LoginEntry.SCOLUMN_PASSWORD + " TEXT NOT NULL, "
                    + LoginEntry.SCOLUMN_EMAIL + " TEXT NOT NULL, "
                    + LoginEntry.SCOLUMN_PHONE + " TEXT NOT NULL);";
            db.execSQL(CREATE_STUDENT_TABLE);
            db.execSQL("INSERT INTO " + LoginEntry.STABLE_NAME+ "(name,class,roll,username,password,email_id,phone_no) VALUES('Simran','CO5G','10','Simran','Simran123','simran@gmail.com','1234567890')");

            String CREATE_MARKS_TABLE = "CREATE TABLE " +LoginEntry.MTABLE_NAME +"("
                    + LoginEntry._MID + " INTEGER NOT NULL, "
                    + LoginEntry.MCOLUMN_EXAM + " TEXT NOT NULL, "
                    + LoginEntry.MCOLUMN_SUBJECT + " TEXT NOT NULL, "
                    + LoginEntry.MCOLUMN_CLASS + " TEXT NOT NULL, "
                    + LoginEntry.MCOLUMN_NAME + " TEXT NOT NULL, "
                    + LoginEntry.MCOLUMN_ROLL + " INTEGER NOT NULL, "
                    + LoginEntry.MCOLUMN_EMAIL + " TEXT NOT NULL, "
                    + LoginEntry.MCOLUMN_MARKS + " INTEGER NOT NULL );";
            db.execSQL(CREATE_MARKS_TABLE);
            db.execSQL("INSERT INTO " + LoginEntry.MTABLE_NAME+ "(_id,exam,class,subject,name,roll,email_id,marks) VALUES('1','CT1','CO5G','JAVA','Simran','10','simran@gmail.com','12')");

            String CREATE_ATTENDANCE_TABLE = "CREATE TABLE " +LoginEntry.ATTABLE_NAME +"("
                    + LoginEntry._ATID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + LoginEntry.ATCOLUMN_NAME + " TEXT NOT NULL, "
                    + LoginEntry.ATCOLUMN_CLASS + " TEXT NOT NULL, "
                    + LoginEntry.ATCOLUMN_ROLL + " TEXT NOT NULL, "
                    + LoginEntry.ATCOLUMN_ATTENDANCE + " TEXT NOT NULL DEFAULT PRESENT,"
                    + LoginEntry.ATCOLUMN_DATE + " TEXT,"
                    + LoginEntry.ATCOLUMN_MONTH + " TEXT);";
            db.execSQL(CREATE_ATTENDANCE_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + LoginEntry.ATABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + LoginEntry.TTABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + LoginEntry.STABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + LoginEntry.MTABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + LoginEntry.ATTABLE_NAME);
            // Create tables again
            onCreate(db);
        }
}
