package com.example.android.login;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.login.data.LoginContract;
import com.example.android.login.data.LoginDbHelper;

import javax.security.auth.Subject;

public class AddMarks extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor>{
    private Uri mCurrentStudentUri;
    private static final int EXISTING_MARK_LOADER = 0;
    private EditText mName;
    private EditText mRoll;
    private EditText mEmail;
    private EditText mMark;
    private EditText mId;
    private EditText mClass;
    private EditText mSubject;
    private EditText mExam;
    String phoneString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_marks);

        Intent intent = getIntent();
        mCurrentStudentUri = intent.getData();
        getLoaderManager().initLoader(EXISTING_MARK_LOADER, null, this);


        Bundle b=getIntent().getExtras();
        // Find all relevant views that we will need to read user input from
        mName = (EditText) findViewById(R.id.et_name);
        mName.setEnabled(false);
        mRoll = (EditText) findViewById(R.id.et_roll);
        mEmail = (EditText) findViewById(R.id.et_Email);
        mMark = (EditText) findViewById(R.id.et_Marks);
        mId = (EditText) findViewById(R.id.et_id);
        mClass = (EditText)findViewById(R.id.et_class);
        mSubject =(EditText) findViewById(R.id.et_subject);
        mExam = (EditText)findViewById(R.id.et_exam);
        mClass = (EditText) findViewById(R.id.et_class);

        mSubject.setText(b.getCharSequence("Subject"));
        mClass.setText(b.getCharSequence("Class"));
        mExam.setText(b.getCharSequence("Exam"));
    }

    public void insertStudent() {
        String nameString = mName.getText().toString().trim();
        String rollString = mRoll.getText().toString().trim();
        int roll = Integer.parseInt(rollString);
        String emailString = mEmail.getText().toString().trim();
        String markString = mMark.getText().toString().trim();
        int marks = Integer.parseInt(markString);
        String idString = mId.getText().toString().trim();
        int id = Integer.parseInt(idString);
        String subj = mSubject.getText().toString().trim();
        String section = mClass.getText().toString().trim();
        String exam = mExam.getText().toString().trim();

        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Create a ContentValues object where column names are the keys,
        // and Toto's pet attributes are the values.
        ContentValues values = new ContentValues();
        values.put(LoginContract.LoginEntry.MCOLUMN_NAME, nameString);
        values.put(LoginContract.LoginEntry.MCOLUMN_ROLL, roll);
        values.put(LoginContract.LoginEntry.MCOLUMN_EMAIL, emailString);
        values.put(LoginContract.LoginEntry.MCOLUMN_MARKS, marks);
        values.put(LoginContract.LoginEntry._MID,id);
        values.put(LoginContract.LoginEntry.MCOLUMN_EXAM,exam);
        values.put(LoginContract.LoginEntry.MCOLUMN_SUBJECT, subj);
        values.put(LoginContract.LoginEntry.MCOLUMN_CLASS,section);
        long newRowId = db.insert(LoginContract.LoginEntry.MTABLE_NAME, null, values);
        Log.v("AddMarks", "New Row ID:" + newRowId);
         String textThatYouWantToShare =
                "Marks obtained by " + nameString + " of class " + section + " in exam " + exam +" in subject "+subj + " is " + marks;

        // COMPLETED (6) Replace the Toast with shareText, passing in the String from step 5
        /* Send that text to our method that will share it. */
        checkName(nameString,textThatYouWantToShare);
        // COMPLETED (6) Replace the Toast with shareText, passing in the String from step 5
        /* Send that text to our method that will share it. */
        // shareText(textThatYouWantToShare);
    }
     /*  private void shareText(String textToShare) {
        String mimeType = "text/plain";
        String title = "Learning How to Share";
        ShareCompat.IntentBuilder
                /* The from method specifies the Context from which this share is coming from
                    .from(this)
                  .setType(mimeType)
                .setChooserTitle(title)
               .setText(textToShare)
            .startChooser();
    }*/

    public void checkName(String name,String messageString){
        LoginDbHelper mDbHelper = new LoginDbHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT phone_no from STUDENT WHERE name='" + name + "'", null);

        // if Cursor is contains results
        if (cursor != null) {
            // move cursor to first row
            if (cursor.moveToFirst()) {
                // do {
                // Get version from Cursor
                phoneString = cursor.getString(cursor.getColumnIndex("phone_no"));
                composeMmsMessage(messageString, phoneString);
                // move to next row
                // } while (cursor.moveToNext());
            }
        }
    }

    public void composeMmsMessage(String message,String phoneNumber ) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Since the editor shows all pet attributes, define a projection that contains
        // all columns from the pet table
        // you will actually use after this query.
        String[] projection = {
                LoginContract.LoginEntry._SID,
                LoginContract.LoginEntry.SCOLUMN_NAME,
                LoginContract.LoginEntry.SCOLUMN_ROLL,
                LoginContract.LoginEntry.SCOLUMN_EMAIL,
                LoginContract.LoginEntry.SCOLUMN_PHONE};
// This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                mCurrentStudentUri,         // Query the content URI for the current pet
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
// Bail early if the cursor is null or there is less than 1 row in the cursor
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        // Proceed with moving to the first row of the cursor and reading data from it
        // (This should be the only row in the cursor)
        if (cursor.moveToFirst()) {
            // Find the columns of pet attributes that we're interested in
            int idColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry._SID);
            int nameColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_NAME);
            int rollColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_ROLL);
            int emailColumnIndex = cursor.getColumnIndex(LoginContract.LoginEntry.SCOLUMN_EMAIL);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            int roll=cursor.getInt(rollColumnIndex);
            String email = cursor.getString(emailColumnIndex);
            int id=cursor.getInt(idColumnIndex);

            // Update the views on the screen with the values from the database
            mName.setText(name);
            mRoll.setText(Integer.toString(roll));
            mEmail.setText(email);
            mId.setText(Integer.toString(id));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // If the loader is invalidated, clear out all the data from the input fields.
        mName.setText("");
        mRoll.setText("");
        mEmail.setText("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_addstudent, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                insertStudent();
                //Exit activity
                finish();
                // Do nothing for now
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                // Do nothing for now
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
